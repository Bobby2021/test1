package tests;

import java.net.MalformedURLException;
import java.net.URL;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.testng.annotations.*;
import io.appium.java_client.remote.MobileCapabilityType;
import io.appium.java_client.MobileElement;
import io.appium.java_client.AppiumDriver;

@SuppressWarnings("rawtypes")
public class MobileClass {
	
	static AppiumDriver<MobileElement> driver;
	
	public static void main(String args[])
	{
		try {
			setup();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println(e.getCause());
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
	}
	
	//@BeforeTest
	
	public static void setup() throws Exception {
		
		DesiredCapabilities caps = new DesiredCapabilities();
		//caps.setCapability("platformName", "ANDROID");
		//caps.setCapability(CapabilityType.PLATFORM_NAME,"ANDROID");
		caps.setCapability(MobileCapabilityType.PLATFORM_NAME,"ANDROID");
		caps.setCapability(MobileCapabilityType.PLATFORM_VERSION,"11");
		caps.setCapability(MobileCapabilityType.DEVICE_NAME,"Pixel 4a");
		caps.setCapability(MobileCapabilityType.UDID,"07151JEC203175");
		caps.setCapability(MobileCapabilityType.NEW_COMMAND_TIMEOUT,60);
		caps.setCapability("automationName", "UiAutomator2"); 
		//caps.setCapability(MobileCapabilityType.APP,""); //using native client app to test then use apk files relative path
		//caps.setCapability(MobileCapabilityType.BROWSER_NAME,"CHROME");
		caps.setCapability("appPackage","com.google.android.calculator");
		caps.setCapability("appActivity","com.android.calculator2.Calculator");
		URL url = new URL("http://127.0.0.1:4723/wd/hub");
		driver = new AppiumDriver<MobileElement>(url,caps);
		System.out.println("Application Started.......");
		
	}
	
	//@AfterTest
	public void teardown() {
		
	}

}
