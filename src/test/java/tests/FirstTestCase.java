package tests;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.ITestResult;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;


import pageObject.Registrationform;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFCell;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;
import org.apache.poi.ss.usermodel.DateUtil;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class FirstTestCase {

	public static WebDriver driver;
	public static Registrationform lp;
	public ExtentHtmlReporter htmlrpt;
	static ExtentReports extent;
	static ExtentTest test;
	
	@BeforeTest
	public void startReport()
	{
		
		htmlrpt= new ExtentHtmlReporter(System.getProperty("user.dir")+"/target/ExtentReport.html");
		htmlrpt.config().setDocumentTitle("Automation");
		htmlrpt.config().setReportName("FuncRpt");
		htmlrpt.config().setTheme(Theme.DARK);
		
		extent = new ExtentReports();
		extent.attachReporter(htmlrpt);
		extent.setSystemInfo("Hostnmame", "Localhost");
		extent.setSystemInfo("Environment", "TestQA");
		extent.setSystemInfo("Browser", "Chrome");
		
//		extent = new ExtentReports(System.getProperty("user.dir")+"/target/ExtentReport.html",true);
//		extent.addSystemInfo("Hostnmame", "Localhost");
//		extent.addSystemInfo("Environment", "TestQA");
//		extent.loadConfig(new File(System.getProperty("user.dir")+"\\extent-config.xml"));
//		
	}
	
	@AfterTest
	public void endreport()
	{
		extent.flush();
	}
	@Test(groups= {"Sanity","Regression"})	
	public static void readExcel() throws IOException {
		// TODO Auto-generated method stub
		
		test = extent.createTest("readExcel");
				
		//Create an object of File class to open xlsx file
		File file = new File("C:\\Eclipseworkspace\\MobileAuto\\src\\test\\resources\\InputData\\InputData.xlsx");
		
		//Create an object of FileInputStream class to read excel file
		FileInputStream inputStream = new FileInputStream(file);

		//creating workbook instance that refers to .xlsx file
		XSSFWorkbook wb = new XSSFWorkbook(inputStream);

		//creating a Sheet object
		XSSFSheet sheet1=wb.getSheet("DataIn");
		//Create a row object to retrieve row at index 2
		XSSFRow row=sheet1.getRow(2);
		//Create a cell object to retrieve cell at index 2
		XSSFCell cell=row.getCell(2);
		//Get the email address in a variable
		String email= cell.getStringCellValue();
		//Printing the email address
		System.out.println("Email is :"+ email);

		int rowCount=sheet1.getPhysicalNumberOfRows();
		int cellcount=row.getPhysicalNumberOfCells();
		int lastrow=sheet1.getLastRowNum();
		int getLastCell = row.getLastCellNum();
		System.out.println("First row num is "+sheet1.getFirstRowNum());
		System.out.println("Last row num is "+lastrow);   
		System.out.println("Row count is "+rowCount); //get row count in a row
		System.out.println("First col num is "+row.getFirstCellNum());
		System.out.println("Last col num is "+getLastCell);
		System.out.println("Column count is "+cellcount);   //get cell count in a row
		try
		{
			//iterate over all the row to print the data present in each cell.
			for(int i=0;i<=lastrow;i++)
			{
				row=sheet1.getRow(i);

				//iterate over each cell to print its value
				System.out.println("Row "+ i+" data is :");

				for(int j=0;j<cellcount;j++)
				{
					cell=row.getCell(j);
					switch(cell.getCellType())
					{
					case STRING:System.out.print(cell.getStringCellValue() +",");break;
					case NUMERIC:
						if(DateUtil.isCellDateFormatted(cell)){
							System.out.print(cell.getDateCellValue()+",");break;
						}
						else {
							System.out.print(cell.getNumericCellValue() +",");break;
						}
					case BOOLEAN:System.out.print(cell.getBooleanCellValue() +",");break;
					case FORMULA:System.out.print(cell.getCellFormula() +",");break;
					default:
						break;
					}
					//System.out.print(sheet1.getRow(i).getCell(j).getStringCellValue() +",");
					//System.out.print(cell.getStringCellValue() +",");
				}
				System.out.println();
			}
		}
		catch(NullPointerException e)
		{
			e.printStackTrace();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		wb.close();
		inputStream.close();
		
		Assert.assertTrue(true);
		//test.log(LogStatus.PASS,"Assert Pass as condition is True");

	}

	public static void WriteExcel() throws IOException {
		// TODO Auto-generated method stub

		//Create an object of File class to open xlsx file
		File file = new File("C:\\Eclipseworkspace\\MobileAuto\\src\\test\\resources\\InputData\\InputData.xlsx");

		//Create an object of FileInputStream class to read excel file
		FileInputStream inputStream = new FileInputStream(file);

		//creating workbook instance that refers to .xlsx file
		XSSFWorkbook wb = new XSSFWorkbook(inputStream);

		//creating a Sheet object
		XSSFSheet sheet1=wb.getSheet("DataIn");
		XSSFSheet sheet2=wb.createSheet("DataOut");
		XSSFSheet sheet3=wb.createSheet("DataOut2");
		//Create a row object to retrieve row at index 2
		XSSFRow row=sheet1.getRow(1);

		int rowCount=sheet1.getPhysicalNumberOfRows();
		int cellcount=row.getPhysicalNumberOfCells();
		int lastrow=sheet1.getLastRowNum();
		int getLastCell = row.getLastCellNum();
		//				System.out.println("First row num is "+sheet1.getFirstRowNum());
		//				System.out.println("Last row num is "+lastrow);   
		//				System.out.println("Row count is "+rowCount); //get row count in a row
		//				System.out.println("First col num is "+row.getFirstCellNum());
		//				System.out.println("Last col num is "+getLastCell);
		//				System.out.println("Column count is "+cellcount);   //get cell count in a row
		try
		{
			//iterate over all the row to print the data present in each cell.
			for(int i=0;i<=lastrow;i++)
			{
				row=sheet1.getRow(i);

				//iterate over each cell to print its value
				//System.out.println("Row"+ i+" data is :");

				for(int j=0;j<cellcount;j++)
				{
					XSSFCell cell=row.getCell(j);
					switch(cell.getCellType())
					{
					case STRING:cell.getStringCellValue();break;
					case NUMERIC:
						if(DateUtil.isCellDateFormatted(cell)){
							cell.getDateCellValue();break;
						}
						else {
							cell.getNumericCellValue();break;
						}
					case BOOLEAN:cell.getBooleanCellValue();break;
					case FORMULA:cell.getCellFormula();break;
					default:
						break;
					}

					FileOutputStream outputStream =new FileOutputStream(new File("C:\\Eclipseworkspace\\MobileAuto\\src\\test\\resources\\InputData\\InputData1.xlsx"));
					wb.write(outputStream);
					outputStream.close();
				}
				//System.out.println();
			}
		}
		catch(NullPointerException e)
		{
			e.printStackTrace();
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}

		wb.close();
		inputStream.close();
		System.out.println("Data written successfully");

	}

	@Test(groups= {"Regression"})
	public static void WriteToForm() throws IOException {
		
		test = extent.createTest("WriteToForm");
		Logger log = Logger.getLogger("FirstTestcase");
		DOMConfigurator.configure("log4j.xml");
		
		log.info("Broswer Launched");
		System.out.println("Browser is Launched");
		//set the ChromeDriver path
		System.setProperty("webdriver.chrome.driver","C:\\Eclipseworkspace\\MobileAuto\\src\\test\\resources\\drivers\\chromedriver.exe");
		
		
		//Creating an object of ChromeDriver
		driver=new ChromeDriver();
		lp=new Registrationform(driver);
		
		//log.info("Navigated to site");
		//Navigate to the URL
		driver.navigate().to("https://demoqa.com/automation-practice-form");
		
		driver.manage().timeouts().implicitlyWait(40,TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(40,TimeUnit.SECONDS);
		driver.manage().window().maximize();
		
		//Create an object of File class to open xlsx file
		File file =  new File("C:\\Eclipseworkspace\\MobileAuto\\src\\test\\resources\\InputData\\FormData.xlsx");

		//Create an object of FileInputStream class to read excel file
		FileInputStream inputStream = new FileInputStream(file);

		//creating workbook instance that refers to .xlsx file
		XSSFWorkbook wb=new XSSFWorkbook(inputStream);

		//creating a Sheet object
		XSSFSheet sheet=wb.getSheet("DataIn");
		
		//get all rows in the sheet
		int rowCount=sheet.getLastRowNum()-sheet.getFirstRowNum();
		System.out.println("No of row in excel "+ rowCount);
		
		
		
		//Identify the WebElements for the student registration form
//		
//			driver.findElement(By.id("firstName"));
//			driver.findElement(By.id("lastName"));
//			driver.findElement(By.id("userEmail"));
//			driver.findElement(By.id("gender-radio-1"));
//			driver.findElement(By.id("userNumber"));
//			driver.findElement(By.id("dateOfBirthInput"));
//			driver.findElement(By.id("currentAddress"));
//			driver.findElement(By.id("submit"));
			WebElement genderMale=driver.findElement(By.id("gender-radio-1"));
//			WebElement submitBtn=driver.findElement(By.id("submit"));
			WebElement dob=driver.findElement(By.id("dateOfBirthInput"));
			XSSFCell cell = sheet.getRow(0).createCell(7);
			cell.setCellValue("TEST_STATUS");
		//iterate over all the rows in Excel and put data in the form.
			for(int i=1;i<=rowCount;i++) 
			{
			
				XSSFRow current_row = sheet.getRow(i);
			
				String fname=current_row.getCell(0).getStringCellValue();
				String lname=current_row.getCell(1).getStringCellValue();
				String emaill=current_row.getCell(2).getStringCellValue();
				int mobil=(int)current_row.getCell(4).getNumericCellValue(); //type casting
				
				//String mobil=current_row.getCell(4).getStringCellValue();
				String addres=current_row.getCell(6).getStringCellValue();
			
			//Enter the values read from Excel in firstname,lastname,mobile,email,address
				lp.setFirstName(fname);
				lp.setLastName(lname);
				lp.setEmail(emaill);
				lp.setMobile(mobil);
				lp.setAddress(addres);
						
			//Click on the gender radio button using javascript
				JavascriptExecutor js = (JavascriptExecutor) driver;
				js.executeScript("arguments[0].click();", genderMale);
				
			//Click on submit button
				lp.submit();
				
			//Verify the confirmation message
				//lp.setConfMsg();
				//WebElement confirmationMessage = driver.findElement(By.xpath("//div[text()='Thanks for submitting the form']"));

			//create a new cell in the row at index 6
				cell = sheet.getRow(i).createCell(7);

			//check if confirmation message is displayed
			if (lp.setConfMsg()) {
				// if the message is displayed , write PASS in the excel sheet
					cell.setCellValue("PASS");

			} else {
				//if the message is not displayed , write FAIL in the excel sheet
					cell.setCellValue("FAIL");
			}
			System.out.println("Registration submitted for "+i+ " record and the status is "+cell);

			// Write the data back in the Excel file
				FileOutputStream outputStream = new FileOutputStream("C:\\Eclipseworkspace\\MobileAuto\\src\\test\\resources\\InputData\\FormData.xlsx");
				wb.write(outputStream);
				outputStream.close();

			//close the confirmation popup
				//WebElement closebtn = driver.findElement(By.id("closeLargeModal"));
				//closebtn.click();
				
				lp.close();

			//wait for page to come back to registration page after close button is clicked
			driver.manage().timeouts().implicitlyWait(2000, TimeUnit.MILLISECONDS);
		}
		System.out.println("Form submitted successfully");
		//Close the workbook
		wb.close();
		inputStream.close();

		//Quit the driver
		driver.quit();
		
	}
	@AfterMethod
	public void getResult(ITestResult rs) throws IOException
	{
		if (rs.getStatus()==ITestResult.FAILURE)
		{
			test.log(Status.FAIL, " Test case Failed is " + rs.getName());
			test.log(Status.FAIL, " Test case Failed is " + rs.getThrowable());
			String screenshotPath = FirstTestCase.getScreenshot(driver, rs.getName());
			test.addScreenCaptureFromPath(screenshotPath);
		} else if (rs.getStatus()==ITestResult.SKIP)
		{
			test.log(Status.SKIP, " Test case Skipped is " + rs.getName());
			
		}
		else if (rs.getStatus()==ITestResult.SUCCESS)
		{
			test.log(Status.PASS, " Test case Passed is " + rs.getName());
			
		}
		//driver.close();
	}
	
	public static String getScreenshot(WebDriver driver, String screenshotName) throws IOException {
		// TODO Auto-generated method stub
		String dateName = new SimpleDateFormat("yyyyMMddhhmmss").format(new Date());
		TakesScreenshot ts = (TakesScreenshot) driver;
		File source=ts.getScreenshotAs(OutputType.FILE);
		
		String destination = System.getProperty("user.dir") + "/src/test/resources/Results/" + screenshotName + dateName + ".png";
		File fdestination = new File(destination);
		FileUtils.copyFile(source, fdestination);
		return destination;
		
	}
	
	public static void main(String args[]) throws IOException
	{
		readExcel();
		//WriteExcel();
		WriteToForm();
		
	}

	
}
