package pageObject;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.CacheLookup;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class Registrationform {

	public WebDriver ldriver;
	public Registrationform(WebDriver rdriver)
	{
		ldriver=rdriver;
		PageFactory.initElements(rdriver, this);
			
	}
	@FindBy(id="firstName")
	@CacheLookup
	WebElement firstName;
	
	@FindBy(id="lastName")
	@CacheLookup
	WebElement lastName;
	
	@FindBy(id="userEmail")
	@CacheLookup
	WebElement email;

	@FindBy(id="gender-radio-1")
	@CacheLookup
	WebElement genderMale;
	
	@FindBy(id="userNumber")
	@CacheLookup
	WebElement mobile;
	
	@FindBy(id="dateOfBirthInput")
	@CacheLookup
	WebElement dob;
	
	@FindBy(id="currentAddress")
	@CacheLookup
	WebElement address;
	
	@FindBy(id="submit")
	@CacheLookup
	WebElement submitBtn;
	
	@FindBy(xpath="//div[text()='Thanks for submitting the form']")
	WebElement confirmationMessage;
	
	@FindBy(id="closeLargeModal")
	WebElement closebtn;
	
	@FindBy(linkText="Logout")
	@CacheLookup
	WebElement lnkLogout;
	
	public void setFirstName(String fname)
	{
		firstName.clear();
		firstName.sendKeys(fname);
	}
	public void setLastName(String lname)
	{
		lastName.clear();
		lastName.sendKeys(lname);
	}
	public void setEmail(String emaill)
	{
		email.clear();
		email.sendKeys(emaill);
	}
	public void setMobile(int mobil)
	{
		mobile.clear();
		mobile.sendKeys(String.valueOf(mobil));
	}
	
	public void setAddress(String addres)
	{
		address.clear();
		address.sendKeys(addres);
	}
	
	public void clickGender()
	
	{
		genderMale.click();
	}
	
	public void submit()
	{
		submitBtn.click();
	}
	
	public boolean setConfMsg()
	{
		return confirmationMessage.isDisplayed();
	}
	
	public void close()
	{
		closebtn.click();
	}
//	
//	public void setPassword(String pwd)
//	{
//		txtPassword.clear();
//		txtPassword.sendKeys(pwd);
//	}
//	
//	public void clickLogin()
//	{
//		btnLogin.click();
//	}
//	
//	public void clickLogout()
//	{
//		lnkLogout.click();
//	}


}
